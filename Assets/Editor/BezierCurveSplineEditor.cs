using System;
using System.Collections.Generic;
using System.Linq;
using TNRD.Utilities;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BezierCurveSpline))]
public class BezierCurveSplineEditor : Editor
{
    private void OnAddButtonPressed(BezierCurve.Mode currentMode)
    {
        var bezierCurveSpline = (BezierCurveSpline) target;
        bezierCurveSpline.SyncCurve();

        switch (currentMode)
        {
            case BezierCurve.Mode.Cubic:
                switch (bezierCurveSpline.curve.points.Count)
                {
                    case 0:
                        var p0 = new GameObject("p0");
                        p0.SetIcon(ShapeIcon.DiamondBlue);
                        p0.transform.parent = bezierCurveSpline.transform;
                        p0.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p0.transform);
                        break;
                    default:
                        var pointsCount = bezierCurveSpline.curve.points.Count;
                        var pointsLast = bezierCurveSpline.curve.points.Last();

                        var p1 = new GameObject($"p{pointsCount}");
                        p1.SetIcon(ShapeIcon.CircleBlue);
                        p1.transform.parent = bezierCurveSpline.transform;
                        p1.transform.position = pointsLast.position;
                        bezierCurveSpline.curve.points.Add(p1.transform);

                        var p2 = new GameObject($"p{pointsCount + 1}");
                        p2.SetIcon(ShapeIcon.CircleBlue);
                        p2.transform.parent = bezierCurveSpline.transform;
                        p2.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p2.transform);

                        var p3 = new GameObject($"p{pointsCount + 2}");
                        p3.SetIcon(ShapeIcon.DiamondBlue);
                        p3.transform.parent = bezierCurveSpline.transform;
                        p3.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p3.transform);
                        break;
                }

                break;
            case BezierCurve.Mode.CubicLoop:
                switch (bezierCurveSpline.curve.points.Count)
                {
                    case 0:
                    {
                        var p0 = new GameObject("p0");
                        p0.SetIcon(ShapeIcon.DiamondBlue);
                        p0.transform.parent = bezierCurveSpline.transform;
                        p0.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p0.transform);
                    }
                        break;
                    case 1:
                    {
                        var pointsFist = bezierCurveSpline.curve.points.First();

                        var p1 = new GameObject("p1");
                        p1.SetIcon(ShapeIcon.CircleBlue);
                        p1.transform.parent = bezierCurveSpline.transform;
                        p1.transform.position = pointsFist.position;
                        bezierCurveSpline.curve.points.Add(p1.transform);

                        var p2 = new GameObject("p2");
                        p2.SetIcon(ShapeIcon.CircleBlue);
                        p2.transform.parent = bezierCurveSpline.transform;
                        p2.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p2.transform);

                        var p3 = new GameObject("p3");
                        p3.SetIcon(ShapeIcon.DiamondBlue);
                        p3.transform.parent = bezierCurveSpline.transform;
                        p3.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p3.transform);

                        var p4 = new GameObject("p4");
                        p4.SetIcon(ShapeIcon.CircleBlue);
                        p4.transform.parent = bezierCurveSpline.transform;
                        p4.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(p4.transform);

                        var p5 = new GameObject("p5");
                        p5.SetIcon(ShapeIcon.CircleBlue);
                        p5.transform.parent = bezierCurveSpline.transform;
                        p5.transform.position = pointsFist.position;
                        bezierCurveSpline.curve.points.Add(p5.transform);
                    }
                        break;
                    default:
                    {
                        var pointsCount = bezierCurveSpline.curve.points.Count;
                        var pointsFist = bezierCurveSpline.curve.points.First();
                        var pointsLast = bezierCurveSpline.curve.points.Last();
                        var pointsLastPosition = pointsLast.position;

                        pointsLast.position = bezierCurveSpline.transform.position;

                        var pn = new GameObject($"p{pointsCount}");
                        pn.SetIcon(ShapeIcon.DiamondBlue);
                        pn.transform.parent = bezierCurveSpline.transform;
                        pn.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(pn.transform);

                        var pn1 = new GameObject($"p{pointsCount + 1}");
                        pn1.SetIcon(ShapeIcon.CircleBlue);
                        pn1.transform.parent = bezierCurveSpline.transform;
                        pn1.transform.position = bezierCurveSpline.transform.position;
                        bezierCurveSpline.curve.points.Add(pn1.transform);

                        var pn2 = new GameObject($"p{pointsCount + 2}");
                        pn2.SetIcon(ShapeIcon.CircleBlue);
                        pn2.transform.parent = bezierCurveSpline.transform;
                        pn2.transform.position = pointsLastPosition;
                        bezierCurveSpline.curve.points.Add(pn2.transform);
                    }
                        break;
                }

                break;
            case BezierCurve.Mode.Linear:
                break;
            case BezierCurve.Mode.LinearLoop:
                break;
            case BezierCurve.Mode.Quadratic:
                break;
            case BezierCurve.Mode.QuadraticLoop:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public override void OnInspectorGUI()
    {
        var bezierCurveSpline = (BezierCurveSpline) target;
        var currentMode = bezierCurveSpline.curve.currentMode;
        var currentModeIsLoop = currentMode.IsLoop();
        EditorGUILayout.LabelField("Current mode", currentMode.ToString());
        EditorGUILayout.LabelField("Segment count", bezierCurveSpline.curve.SegmentCount.ToString());
        if (GUILayout.Button(currentModeIsLoop ? "Set Non Loop" : "Set Loop"))
        {
            //OnLoopButtonPressed(currentMode, currentModeIsLoop);
            OnModeButtonPressed(currentModeIsLoop ? currentMode.GetNonLoop() : currentMode.GetLoop());
        }

        if (currentMode != BezierCurve.Mode.Cubic && currentMode != BezierCurve.Mode.CubicLoop)
        {
            if (GUILayout.Button(currentModeIsLoop ? "Set CubicLoop" : "Set Cubic"))
            {
                OnModeButtonPressed(currentModeIsLoop ? BezierCurve.Mode.CubicLoop : BezierCurve.Mode.Cubic);
            }
        }

        /*if (currentMode != BezierCurve.Mode.Linear && currentMode != BezierCurve.Mode.LinearLoop)
        {
            if (GUILayout.Button(currentModeIsLoop ? "Set LinearLoop" : "Set Linear"))
            {
                OnModeButtonPressed(currentModeIsLoop ? BezierCurve.Mode.LinearLoop : BezierCurve.Mode.Linear);
            }
        }

        if (currentMode != BezierCurve.Mode.Quadratic && currentMode != BezierCurve.Mode.QuadraticLoop)
        {
            if (GUILayout.Button(currentModeIsLoop ? "Set QuadraticLoop" : "Set Quadratic"))
            {
                OnModeButtonPressed(currentModeIsLoop ? BezierCurve.Mode.QuadraticLoop : BezierCurve.Mode.Quadratic);
            }
        }*/

        if (GUILayout.Button("Add control point"))
        {
            OnAddButtonPressed(currentMode);
        }

        if (GUILayout.Button("Remove control point"))
        {
            OnRemoveButtonPressed(currentMode);
        }
    }

    /*private void OnLoopButtonPressed(BezierCurve.Mode currentMode, bool currentModeIsLoop)
    {
        var bezierCurveSpline = (BezierCurveSpline) target;
        bezierCurveSpline.Curve.CurrentMode =
            currentModeIsLoop ? currentMode.GetNonLoop() : currentMode.GetLoop();
    }*/

    private void OnModeButtonPressed(BezierCurve.Mode newMode)
    {
        var bezierCurveSpline = (BezierCurveSpline) target;
        bezierCurveSpline.curve.currentMode = newMode;
        while (bezierCurveSpline.transform.childCount != 0)
        {
            DestroyImmediate(bezierCurveSpline.transform.GetChild(0).gameObject);
        }

        bezierCurveSpline.SyncCurve(true);
    }

    private void OnRemoveButtonPressed(BezierCurve.Mode currentMode)
    {
        var bezierCurveSpline = (BezierCurveSpline) target;
        bezierCurveSpline.SyncCurve();

        switch (currentMode)
        {
            case BezierCurve.Mode.Cubic:
                switch (bezierCurveSpline.curve.points.Count)
                {
                    case 0:
                        break;
                    case 1:
                        DestroyImmediate(bezierCurveSpline.curve.points[0].gameObject);
                        bezierCurveSpline.curve.points.RemoveAt(0);
                        break;
                    default:
                        for (var i = 0; i < 3; i++)
                        {
                            DestroyImmediate(bezierCurveSpline.curve.points[bezierCurveSpline.curve.points.Count - 1]
                                .gameObject);
                            bezierCurveSpline.curve.points.RemoveAt(bezierCurveSpline.curve.points.Count - 1);
                        }

                        break;
                }

                break;
            case BezierCurve.Mode.CubicLoop:
                switch (bezierCurveSpline.curve.points.Count)
                {
                    case 0:
                        break;
                    case 1:
                        DestroyImmediate(bezierCurveSpline.curve.points[0].gameObject);
                        bezierCurveSpline.curve.points.RemoveAt(0);
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 6:
                        for (var i = 0; i < 5; i++)
                        {
                            DestroyImmediate(bezierCurveSpline.curve.points[bezierCurveSpline.curve.points.Count - 1]
                                .gameObject);
                            bezierCurveSpline.curve.points.RemoveAt(bezierCurveSpline.curve.points.Count - 1);
                        }

                        break;
                    default:
                        bezierCurveSpline.curve.points[bezierCurveSpline.curve.points.Count - 4].position =
                            bezierCurveSpline.curve.points.Last().position;
                        for (var i = 0; i < 3; i++)
                        {
                            DestroyImmediate(bezierCurveSpline.curve.points[bezierCurveSpline.curve.points.Count - 1]
                                .gameObject);
                            bezierCurveSpline.curve.points.RemoveAt(bezierCurveSpline.curve.points.Count - 1);
                        }

                        break;
                }

                break;
            case BezierCurve.Mode.Linear:
                break;
            case BezierCurve.Mode.LinearLoop:
                break;
            case BezierCurve.Mode.Quadratic:
                break;
            case BezierCurve.Mode.QuadraticLoop:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
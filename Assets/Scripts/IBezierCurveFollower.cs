using UnityEngine;

public interface IBezierCurveFollower
{
    Vector2 CameraRotation { set; }

    BezierCurve Curve { get; set; }

    float CurrentT { get; set; }
}
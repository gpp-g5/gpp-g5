﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    public GameObject cutsceneCamera;
    public GameObject mainCamera;
    public Transform focus;
    public Transform cameraDestination;

    private PlayerController player;

    private void Start()
    {
        player = FindObjectOfType<PlayerController>();

        cutsceneCamera.SetActive(false);
    }

    [Range(1.0f, 10.0f)]
    public float cameraSpeed = 5f;

    private float speedMultiplier = 0.0001f;

    public IEnumerator StartCutscene()
    {
        player.FreezePlayer(true);

        cutsceneCamera.SetActive(true);

        cutsceneCamera.transform.position = mainCamera.transform.position;
        cutsceneCamera.transform.rotation = mainCamera.transform.rotation;

        mainCamera.SetActive(false);

        Quaternion startRot = cutsceneCamera.transform.rotation;
        float y = 0f;
        float startDistance = Vector3.Distance(cutsceneCamera.transform.position, cameraDestination.position);

        while (Vector3.Distance(cutsceneCamera.transform.position, cameraDestination.position) > 0.2f)
        {
            cutsceneCamera.transform.position = Vector3.Lerp(cutsceneCamera.transform.position, cameraDestination.position, y += (cameraSpeed * speedMultiplier));

            float x = 1 - Vector3.Distance(cutsceneCamera.transform.position, cameraDestination.position) / startDistance;
            float lerp = 1 / (1 + Mathf.Exp((-12f * (x - 0.5f))));
            cutsceneCamera.transform.rotation = Quaternion.Lerp(startRot, cameraDestination.transform.rotation, lerp);

            yield return null;
        }

        StarDoor[] starDoors = FindObjectsOfType<StarDoor>();

        foreach(StarDoor i in starDoors)
        {
            i.OpenDoor();
        }

        yield return new WaitForSeconds(2.0f);

        

        startRot = cutsceneCamera.transform.rotation;
        y = 0f;

        startDistance = Vector3.Distance(cutsceneCamera.transform.position, mainCamera.transform.position);
        while (Vector3.Distance(cutsceneCamera.transform.position, mainCamera.transform.position) > 0.2f)
        {
            cutsceneCamera.transform.position = Vector3.Lerp(cutsceneCamera.transform.position, mainCamera.transform.position, y += (cameraSpeed * speedMultiplier));

            float x = 1 - Vector3.Distance(cutsceneCamera.transform.position, mainCamera.transform.position) / startDistance;
            float lerp = 1 / (1 + Mathf.Exp((-12f * (x - 0.5f))));
            cutsceneCamera.transform.rotation = Quaternion.Lerp(startRot, mainCamera.transform.rotation, lerp);

            yield return null;
        }

        ResetCameras();
    }

    public void ResetCameras()
    {
        player.FreezePlayer(false);
        mainCamera.SetActive(true);
        cutsceneCamera.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

    private bool timeToShake;

    public IEnumerator Shake(float duration, float magnitude)
    {
        timeToShake = true;

        Vector3 initialPos = transform.position;

        float elapsedTime = 0.0f;

        while (elapsedTime < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.position = initialPos + new Vector3(x, y, 0);

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        timeToShake = false;
        transform.position = initialPos;

        yield return null;
    }
}

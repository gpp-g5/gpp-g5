using System;
using UnityEngine;
using UnityEngine.UI;

public class ControlledFollower : MonoBehaviour, IBezierCurveFollower
{
    private const float Gravity = 20.0f;
    private CharacterController _characterController;
    private float _horizontal;
    private Vector3 _moveDirection = Vector3.zero;
    private Rigidbody _rigidbody;
    [SerializeField] private float jumpSpeed = 8.0f;
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private BezierCurveSpline spline;
    [SerializeField] private Text text;

    #region IBezierCurveFollower Members

    public Vector2 CameraRotation { get; set; }

    public BezierCurve Curve
    {
        get { return spline.curve; }
        set { throw new Exception("Set is unsupported"); }
    }

    public float CurrentT { get; set; } = 0.0f;

    #endregion

    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (_characterController.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            //_moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            //_moveDirection *= speed;

            _horizontal = Input.GetAxis("Horizontal");

            _moveDirection.y = Input.GetButton("Jump") ? jumpSpeed : 0.0f;
        }

        if (!Utility.FloatCompare(_horizontal, 0.0f))
        {
            CurrentT += _horizontal * speed * Time.deltaTime / spline.curve.GetTangent(CurrentT).magnitude;
            CurrentT = Mathf.Clamp(CurrentT, 0.0f, spline.curve.SegmentCount);
        }

        var move = spline.curve.GetPosition(CurrentT) - transform.position;
        _moveDirection = new Vector3(move.x, _moveDirection.y, move.z);

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        _moveDirection.y -= Gravity * Time.deltaTime;

        // Move the controller
        _characterController.Move(new Vector3(_moveDirection.x, _moveDirection.y * Time.deltaTime, _moveDirection.z));

        text.text = $"{CurrentT}, {_moveDirection}";
    }
}
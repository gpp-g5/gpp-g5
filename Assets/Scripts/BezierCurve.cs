using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BezierCurve
{
    #region Mode enum

    public enum Mode
    {
        Cubic,
        CubicLoop,
        Linear,
        LinearLoop,
        Quadratic,
        QuadraticLoop
    }

    #endregion

    public Mode currentMode;
    public List<Transform> points = new List<Transform>();

    public BezierCurve(Mode mode)
    {
        currentMode = mode;
    }

    public int SegmentCount
    {
        get
        {
            int result;
            switch (currentMode)
            {
                case Mode.Cubic:
                case Mode.CubicLoop:
                    result = points.Count / 3;
                    break;
                case Mode.Linear:
                    result = points.Count - 1;
                    break;
                case Mode.LinearLoop:
                    result = points.Count;
                    break;
                case Mode.Quadratic:
                case Mode.QuadraticLoop:
                    result = points.Count / 2;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result >= 0 ? result : 0;
        }
    }

    public Vector3 GetPosition(float t)
    {
        var tFloor = Mathf.Floor(t);
        var segment = (int) tFloor;
        float segmentT;

        if (segment == SegmentCount)
        {
            segment--;
            segmentT = t;
        }
        else
        {
            segmentT = t - tFloor;
        }

        switch (currentMode)
        {
            case Mode.Cubic:
                return GetPositionCubic(segment, segmentT);
            case Mode.CubicLoop:
                return GetPositionCubicLoop(segment, segmentT);
            case Mode.Linear:
                return GetPositionLinear(segment, segmentT);
            case Mode.LinearLoop:
                return GetPositionLinearLoop(segment, segmentT);
            case Mode.Quadratic:
                return GetPositionQuadratic(segment, segmentT);
            case Mode.QuadraticLoop:
                return GetPositionQuadraticLoop(segment, segmentT);
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private Vector3 GetPositionCubic(int segment, float segmentT)
    {
        var p0Index = segment * 3;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2].position;
        var p3 = points[p0Index + 3].position;

        return GetPositionCubic(p0, p1, p2, p3, segmentT);
    }

    private static Vector3 GetPositionCubic(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        // (1 - t)
        var oSt = 1.0f - t;
        // (1 - t)^3
        var a0 = Mathf.Pow(oSt, 3.0f);
        // 3(1 - t)^2 t
        var a1 = 3.0f * Mathf.Pow(oSt, 2.0f) * t;
        // 3(1 - t)t^2
        var a2 = 3.0f * oSt * Mathf.Pow(t, 2.0f);
        // t^3
        var a3 = Mathf.Pow(t, 3);

        return a0 * p0 + a1 * p1 + a2 * p2 + a3 * p3;
    }

    private Vector3 GetPositionCubicLoop(int segment, float segmentT)
    {
        var p0Index = segment * 3;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2].position;
        var p3 = points[p0Index + 3 < points.Count ? p0Index + 3 : 0].position;

        return GetPositionCubic(p0, p1, p2, p3, segmentT);
    }

    private Vector3 GetPositionLinear(int segment, float segmentT)
    {
        return GetPositionLinear(points[segment].position, points[segment + 1].position, segmentT);
    }

    private static Vector3 GetPositionLinear(Vector3 p0, Vector3 p1, float t)
    {
        return p0 + t * (p1 - p0);
    }

    private Vector3 GetPositionLinearLoop(int segment, float segmentT)
    {
        return GetPositionLinear(points[segment].position,
            points[segment + 1 < points.Count ? segment + 1 : 0].position, segmentT);
    }

    private Vector3 GetPositionQuadratic(int segment, float segmentT)
    {
        var p0Index = segment * 2;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2].position;

        return GetPositionQuadratic(p0, p1, p2, segmentT);
    }

    private static Vector3 GetPositionQuadratic(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        // (1 - t)^2
        var a0 = Mathf.Pow(1.0f - t, 2.0f);
        // t^2
        var a1 = Mathf.Pow(t, 2.0f);

        return p1 + a0 * (p0 - p1) + a1 * (p2 - p1);
    }

    private Vector3 GetPositionQuadraticLoop(int segment, float segmentT)
    {
        var p0Index = segment * 2;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2 < points.Count ? p0Index + 2 : 0].position;

        return GetPositionQuadratic(p0, p1, p2, segmentT);
    }

    public Vector3 GetTangent(float t)
    {
        var tFloor = Mathf.Floor(t);
        var segment = (int) tFloor;
        float segmentT;

        if (segment == SegmentCount)
        {
            segment--;
            segmentT = t;
        }
        else
        {
            segmentT = t - tFloor;
        }

        switch (currentMode)
        {
            case Mode.Cubic:
                return GetTangentCubic(segment, segmentT);
            case Mode.CubicLoop:
                return GetTangentCubicLoop(segment, segmentT);
            /*case Mode.Linear:
                return GetTangentLinear(segment, segmentT);
            case Mode.LinearLoop:
                return GetTangentLinearLoop(segment, segmentT);*/
            case Mode.Quadratic:
                return GetTangentQuadratic(segment, segmentT);
            case Mode.QuadraticLoop:
                return GetTangentQuadraticLoop(segment, segmentT);
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private Vector3 GetTangentCubic(int segment, float segmentT)
    {
        var p0Index = segment * 3;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2].position;
        var p3 = points[p0Index + 3].position;

        return GetTangentCubic(p0, p1, p2, p3, segmentT);
    }

    private Vector3 GetTangentCubic(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        return Mathf.Pow(t, 2.0f) * (-3.0f * p0 + 9.0f * p1 - 9.0f * p2 + 3.0f * p3) +
               t * (6.0f * p0 - 12.0f * p1 + 6.0f * p2) + (-3.0f * p0 + 3.0f * p1);
    }

    private Vector3 GetTangentCubicLoop(int segment, float segmentT)
    {
        var p0Index = segment * 3;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2].position;
        var p3 = points[p0Index + 3 < points.Count ? p0Index + 3 : 0].position;

        return GetTangentCubic(p0, p1, p2, p3, segmentT);
    }

    private Vector3 GetTangentQuadratic(int segment, float segmentT)
    {
        var p0Index = segment * 2;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2].position;

        return GetTangentQuadratic(p0, p1, p2, segmentT);
    }

    private Vector3 GetTangentQuadratic(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        return t * (2.0f * p0 - 4.0f * p1 + 2.0f * p2) + (-2.0f * p0 + 2.0f * p1);
    }

    private Vector3 GetTangentQuadraticLoop(int segment, float segmentT)
    {
        var p0Index = segment * 2;
        var p0 = points[p0Index].position;
        var p1 = points[p0Index + 1].position;
        var p2 = points[p0Index + 2 < points.Count ? p0Index + 2 : 0].position;

        return GetTangentQuadratic(p0, p1, p2, segmentT);
    }
}

public static class BezierCurveModeMethods
{
    public static BezierCurve.Mode GetLoop(this BezierCurve.Mode mode)
    {
        switch (mode)
        {
            case BezierCurve.Mode.Cubic:
            case BezierCurve.Mode.CubicLoop:
                return BezierCurve.Mode.CubicLoop;
            case BezierCurve.Mode.Linear:
            case BezierCurve.Mode.LinearLoop:
                return BezierCurve.Mode.LinearLoop;
            case BezierCurve.Mode.Quadratic:
            case BezierCurve.Mode.QuadraticLoop:
                return BezierCurve.Mode.QuadraticLoop;
            default:
                throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
        }
    }

    public static BezierCurve.Mode GetNonLoop(this BezierCurve.Mode mode)
    {
        switch (mode)
        {
            case BezierCurve.Mode.Cubic:
            case BezierCurve.Mode.CubicLoop:
                return BezierCurve.Mode.Cubic;
            case BezierCurve.Mode.Linear:
            case BezierCurve.Mode.LinearLoop:
                return BezierCurve.Mode.Linear;
            case BezierCurve.Mode.Quadratic:
            case BezierCurve.Mode.QuadraticLoop:
                return BezierCurve.Mode.Quadratic;
            default:
                throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
        }
    }

    public static bool IsLoop(this BezierCurve.Mode mode)
    {
        switch (mode)
        {
            case BezierCurve.Mode.Cubic:
            case BezierCurve.Mode.Linear:
            case BezierCurve.Mode.Quadratic:
                return false;
            case BezierCurve.Mode.CubicLoop:
            case BezierCurve.Mode.LinearLoop:
            case BezierCurve.Mode.QuadraticLoop:
                return true;
            default:
                throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
        }
    }

    public static bool IsNonLoop(this BezierCurve.Mode mode)
    {
        switch (mode)
        {
            case BezierCurve.Mode.Cubic:
            case BezierCurve.Mode.Linear:
            case BezierCurve.Mode.Quadratic:
                return true;
            case BezierCurve.Mode.CubicLoop:
            case BezierCurve.Mode.LinearLoop:
            case BezierCurve.Mode.QuadraticLoop:
                return false;
            default:
                throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
        }
    }
}
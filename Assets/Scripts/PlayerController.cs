﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour, IBezierCurveFollower
{
    #region State enum

    public enum State
    {
        Frozen,
        Normal,
        OnSpline
    }

    #endregion

    private const float AttackTime = 1;
    private static readonly int AnimatorJump = Animator.StringToHash("jump");
    private static readonly int AnimatorSpeed = Animator.StringToHash("speed");
    private static readonly int AnimatorAttack = Animator.StringToHash("attack");
    private static readonly int AnimatorAttackAnim = Animator.StringToHash("attackAnim");
    private static readonly int AnimatorHurt = Animator.StringToHash("hurt");
    private static readonly int AnimatorDeath = Animator.StringToHash("death");

    private Animator _animator;

    private CameraController _camera;

    //public bool canMove = true;
    private CharacterController _controller;
    private float _currentSpeed;
    private BezierCurve _curve;

    private float _elapsedAttackTime = 0;
    private float _elapsedInvisTime = 0;

    private float _horizontal;
    private float _speedSmoothVelocity;

    private GameObject _splineCameraTarget;
    private Vector3 _splineMove;

    private SceneTransition _transitionPanel;
    private float _turnSmoothVelocity;
    private float _velocityY;

    [Range(0, 1)] public float airControlPercent;

    //Powerup variables.
    [HideInInspector] public DoubleJump doubleJump;

    [HideInInspector] public float extraJumpCount = 0;
    public float extraJumps = 0;
    public float gravity = -12;


    [HideInInspector] public bool hasSword = false;
    public float health = 5;

    private float invisibilityTime = 1.0f;
    public float jumpHeight = 2;
    public float runSpeed = 6;
    [HideInInspector] public SpeedBoost speedBoost;

    public float speedSmoothTime = 0.1f;
    public State state = State.Normal;
    public GameObject sword;
    [HideInInspector] public SwordPowerUp swordPowerUp;
    public float turnSmoothTime = 0.2f;
    public float walkSpeed = 2;

    #region IBezierCurveFollower Members

    public BezierCurve Curve
    {
        get { return _curve; }
        set
        {
            _camera.input.enableLook = false;
            _camera.orbit.followTarget = true;
            _camera.target = _splineCameraTarget.transform;
            state = State.OnSpline;
            _curve = value;
        }
    }

    public Vector2 CameraRotation
    {
        set { _camera.Rotation = value; }
    }

    public float CurrentT { get; set; }

    #endregion

    // Start is called before the first frame update
    private void Start()
    {
        _animator = GetComponent<Animator>();
        if (Camera.main != null)
        {
            _camera = Camera.main.GetComponent<CameraController>();
        }
        else
        {
            Debug.LogError("Please add a main camera.");
        }

        _controller = GetComponent<CharacterController>();

        _splineCameraTarget = new GameObject();

        _transitionPanel = FindObjectOfType<SceneTransition>();
    }

    // Update is called once per frame
    private void Update()
    {

        switch (state)
        {
            case State.Frozen:
                break;
            case State.Normal:
            {
                //input
                var input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                var inputDir = input.normalized;
                var running = Input.GetButton("Sprint");

                Move(inputDir, running);
                if (Input.GetButtonDown("Jump"))
                {
                    Jump();
                }

                if (Input.GetButtonDown("Attack"))
                {
                    Attack();
                }

                if (!_controller.isGrounded)
                {
                    _animator.SetBool(AnimatorJump, true);
                }
                else
                {
                    _animator.SetBool(AnimatorJump, false);

                    extraJumpCount = extraJumps;
                }

                //animator
                var animationSpeed = running ? _currentSpeed / runSpeed : _currentSpeed / walkSpeed * .5f;
                _animator.SetFloat(AnimatorSpeed, animationSpeed, speedSmoothTime, Time.deltaTime);

                //invincibility timer
                if (_elapsedInvisTime < invisibilityTime)
                {
                    _elapsedInvisTime += Time.deltaTime;
                }

                //attack timer
                if (_elapsedAttackTime < AttackTime)
                {
                    _elapsedAttackTime += Time.deltaTime;
                }
            }
                break;
            case State.OnSpline:
            {

                var running = Input.GetButton("Sprint");

                    if (_controller.isGrounded)
                {
                    _horizontal = -Input.GetAxis("Horizontal");

                    _splineMove.y = Input.GetButton("Jump") ? jumpHeight * 4.0f : 0.0f;

                    _animator.SetBool(AnimatorJump, false);
                }else {
                    _animator.SetBool(AnimatorJump, true);
                }

                if (!Utility.FloatCompare(_horizontal, 0.0f))
                {
                    CurrentT += _horizontal * (running ? runSpeed : walkSpeed) * Time.deltaTime /
                                Curve.GetTangent(CurrentT).magnitude;
                    CurrentT = Mathf.Clamp(CurrentT, 0.0f, Curve.SegmentCount);
                    if (CurrentT >= Curve.SegmentCount)
                    {
                        _camera.input.enableLook = true;
                        _camera.orbit.followTarget = false;
                        _camera.target = transform;
                        state = State.Normal;
                        _curve = null;
                        return;
                    }
                }

                var newPosition = Curve.GetPosition(CurrentT);
                var move = newPosition - transform.position;

                _splineMove = new Vector3(move.x, _splineMove.y, move.z);

                _splineMove.y += gravity * Time.deltaTime;

                _controller.Move(new Vector3(_splineMove.x, _splineMove.y * Time.deltaTime, _splineMove.z));
                transform.rotation = Quaternion.LookRotation(new Vector3(move.x, 0.0f, move.z));

                _currentSpeed = move.magnitude;
                var animationSpeed = running ? _currentSpeed / runSpeed : _currentSpeed / walkSpeed * .5f;
                _animator.SetFloat(AnimatorSpeed, animationSpeed, speedSmoothTime, Time.deltaTime);

                _splineCameraTarget.transform.position = newPosition;
                if (!Utility.FloatCompare(_horizontal, 0.0f))
                {
                    _splineCameraTarget.transform.rotation = Quaternion.LookRotation(_horizontal > 0.0f
                        ? new Vector3(move.x, 0.0f, move.z)
                        : new Vector3(-move.x, 0.0f, -move.z));
                }
            }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Move(Vector2 inputDir, bool running)
    {
        if (inputDir != Vector2.zero && health > 0)
        {
            var targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + _camera.transform.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y,
                targetRotation, ref _turnSmoothVelocity, GetModifiedSmoothTime(turnSmoothTime));
        }

        var targetSpeed = (running ? runSpeed : walkSpeed) * inputDir.magnitude;
        _currentSpeed = Mathf.SmoothDamp(_currentSpeed, targetSpeed, ref _speedSmoothVelocity,
            GetModifiedSmoothTime(speedSmoothTime));

        _velocityY += Time.deltaTime * gravity;
        var velocity = transform.forward * _currentSpeed + Vector3.up * _velocityY;

        _controller.Move(velocity * Time.deltaTime);
        _currentSpeed = new Vector2(_controller.velocity.x, _controller.velocity.z).magnitude;
    }

    private void Jump()
    {
        if (!_controller.isGrounded && !(extraJumpCount > 0)) return;
        var jumpVelocity = Mathf.Sqrt(-2 * gravity * jumpHeight);
        _velocityY = jumpVelocity;

        if (FindObjectOfType<DoubleJump>())
        {
            if (FindObjectOfType<DoubleJump>().hasPowerUp)
            {
                FindObjectOfType<DoubleJump>().SpawnEffect();
            }
        }

        extraJumpCount--;
    }

    private void Attack()
    {
        if (!(_elapsedAttackTime > AttackTime)) return;
        _animator.SetTrigger(AnimatorAttack);

        var randAttackAnim = Random.Range(0.0f, 1.0f);
        _animator.SetFloat(AnimatorAttackAnim, randAttackAnim);

        _elapsedAttackTime = 0;
    }

    //function takes 1 if player has sword and 0 if not.
    public void HasSword(int value)
    {
        var attackLayer = _animator.GetLayerIndex("Attack Layer");

        if (value >= 1)
        {
            _animator.SetLayerWeight(attackLayer, 0);
            sword.SetActive(true);
        }
        else
        {
            _animator.SetLayerWeight(attackLayer, 1);
            sword.SetActive(false);
        }

        var swordBaseLayer = _animator.GetLayerIndex("Sword Layer");
        var swordAttackLayer = _animator.GetLayerIndex("Sword Attack Layer");

        _animator.SetLayerWeight(swordBaseLayer, value);
        _animator.SetLayerWeight(swordAttackLayer, value);
    }

    void Hit()
    {
        //deals with damaging enemies.
        const int damageAmount = 1;
        var hitCheckPosition = transform.position;
        float hitCheckRadius = 3.0f;

        if (hasSword)
        {
            hitCheckRadius = 4.0f;
        }

        var hitColliders = Physics.OverlapSphere(hitCheckPosition, hitCheckRadius);

        foreach (var hit in hitColliders)
        {
            if (!hit) continue;
            if (FindObjectOfType<CameraShake>())
            {
                var cameraShake = FindObjectOfType<CameraShake>();
                StartCoroutine(cameraShake.Shake(0.1f, 0.3f));
            }

            if (hit.GetComponent<Enemy>() && !hit.GetComponent<PlayerController>())
            {
                hit.GetComponent<Enemy>().TakeDamage(damageAmount, hit.transform.position);
            }

            if (hit.GetComponent<Rigidbody>() && hit.gameObject != gameObject)
            {
                ApplyHitForce(hit);
            }
        }
    }

    private void ApplyHitForce(Component hit)
    {
        const int hitForce = 100;
        hit.GetComponent<Rigidbody>().AddForce((hit.transform.position - transform.position) * hitForce);
    }

    private float GetModifiedSmoothTime(float smoothTime)
    {
        if (_controller.isGrounded)
        {
            return smoothTime;
        }

        if (Utility.FloatCompare(airControlPercent, 0.0f))
        {
            return float.MaxValue;
        }

        return smoothTime / airControlPercent;
    }

    public void TakeDamage(float damageAmount)
    {
        var heartUi = FindObjectOfType<HeartUI>();

        if (!(_elapsedInvisTime > invisibilityTime)) return;
        health -= damageAmount;
        _animator.SetTrigger(AnimatorHurt);

        heartUi.UpdateHeartUI();

        if (health <= 0)
        {
            health = 0;
            _animator.SetTrigger(AnimatorDeath);
            _transitionPanel.Respawn();
            FreezePlayer(true);
        }

        _elapsedInvisTime = 0;
    }

    public void FreezePlayer(bool freeze)
    {
        //canMove = !freeze;
        state = freeze ? State.Frozen : State.Normal;

        if (freeze)
        {
            _controller.enabled = false;

            _animator.SetFloat(AnimatorSpeed, 0);
        }
        else
        {
            _controller.enabled = true;
        }
    }

    public void FootR()
    {
        //called in animation, leave this here.
    }

    public void FootL()
    {
        //called in animation, leave this here.
    }

    public void PushButton()
    {
        var doorSwitch = FindObjectOfType<DoorSwitch>();
        doorSwitch.PushButton();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderSpawner : MonoBehaviour
{
    public GameObject boulder;

    private float elapsedTime = 0;
    public float spawnTime = 2;

    private void Update()
    {
        if(elapsedTime > spawnTime)
        {
            Instantiate(boulder, transform.position, Quaternion.identity);
            elapsedTime = 0;
        }
        else
        {
            elapsedTime += Time.deltaTime;
        }
    }
}

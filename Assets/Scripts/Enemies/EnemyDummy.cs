﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDummy : Enemy
{
    private Rigidbody rb;

    public GameObject star;

    public Material activateMat;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        star.SetActive(false);
    }

    public override void OnDamage()
    {
        star.SetActive(true);
        star.transform.parent = null;
        GetComponentInChildren<Renderer>().material = activateMat;
        rb.isKinematic = false;
    }
}

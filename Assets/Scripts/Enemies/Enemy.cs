﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 1;

    public ParticleSystem onDamageEffect;

    public void TakeDamage(int damageAmount, Vector3 particleSpawnPos)
    {
        ParticleSystem ps = Instantiate(onDamageEffect, particleSpawnPos, Quaternion.identity);

        Renderer renderer = GetComponent<Renderer>();

        if (renderer)
        {
            ps.GetComponent<ParticleSystemRenderer>().material = renderer.material;
        }

        health -= damageAmount;

        OnDamage();

        if (health <= 0)
        {
            health = 0;
        }
    }

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

        OnDamage();

        if (health <= 0)
        {
            health = 0;
        }
    }

    public virtual void OnDamage()
    {
        //overriden in child class.
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour
{

    //kill player if they enter this trigger volume.
    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = FindObjectOfType<PlayerController>();

        if(other.gameObject == player.gameObject)
        {
            player.TakeDamage(player.health);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        PlayerController player = FindObjectOfType<PlayerController>();

        if (collision.gameObject == player.gameObject)
        {
            player.TakeDamage(player.health);
        }
    }

}

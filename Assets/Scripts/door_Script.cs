﻿using System.Collections;
using UnityEngine;

public class door_Script : MonoBehaviour
{
    public float timer;
    public float buttonDelay;
    public float cameraDelay;
    public float endDelay;
    public float doorDelay;
    public float transformTime = 2.0f;
    public GameObject button;
    public GameObject door;
    public Camera mainCam;
    public Camera cutSceneCam;

    //public GameObject targetPos;
    public Vector3 targetPos;
    public Vector3 targetRot;


    public GameObject player;
    public PlayerController playerScript;

    private bool triggerEvent = false;
    private bool doorOpen = false;
    private float moveSpeed;

    private float rotateSpeed;

    // Start is called before the first frame update
    void Start()
    {
        playerScript = player.GetComponent<PlayerController>();
        //moveSpeed = Vector3.Distance(this.transform.position, targetPos.transform.position) / transformTime;
        //rotateSpeed = Quaternion.Angle(this.transform.rotation, targetPos.transform.rotation) / transformTime;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Interact") && doorOpen == false)
            {
                Debug.Log("RmB pressed");
                //playerScript.canMove = false;
                playerScript.state = PlayerController.State.Frozen;
                player.GetComponent<Animator>().SetFloat("speedPercent", 0);
                StartCoroutine("ButtonDelay");
                StartCoroutine("CutSceneDelay");
            }
        }
    }

    IEnumerator CloseDoor()
    {
        yield return new WaitForSeconds(timer);
        doorOpen = false;
        Debug.Log("Door Closed");
    }

    IEnumerator ButtonDelay()
    {
        yield return new WaitForSeconds(buttonDelay);
        button.GetComponent<Animation>().Play("ButtonDownUp");
    }

    IEnumerator CutSceneDelay()
    {
        // move player
        player.transform.SetPositionAndRotation(targetPos, Quaternion.Euler(targetRot));


        // disable controller
        player.GetComponent<CharacterController>().enabled = false;
        playerScript.enabled = false;

        // set cameras
        cutSceneCam.enabled = true;
        mainCam.enabled = false;

        // play animation
        playerScript.StartCoroutine("Attack");

        //wait and play animation
        yield return new WaitForSeconds(cameraDelay);
        cutSceneCam.GetComponent<Animation>().Play("doorCutscene");
        yield return new WaitForSeconds(doorDelay);
        door.GetComponent<Animation>().Play("open");
        yield return new WaitForSeconds(endDelay);

        // set cameras
        mainCam.enabled = true;
        cutSceneCam.enabled = false;

        // enable controller
        player.GetComponent<CharacterController>().enabled = true;
        playerScript.enabled = true;
        yield return new WaitForSeconds(1);
        doorOpen = true;
        //playerScript.canMove = true;
        playerScript.state = PlayerController.State.Normal;
    }
}
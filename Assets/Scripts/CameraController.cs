using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    private Camera _camera;
    private Collision _collision;
    private Vector3 _destination = Vector3.zero;
    private Input _input;
    private Orbit _orbit;
    private Position _position;
    private Vector3 _velocity = Vector3.zero;
    public CollisionSettings collision = new CollisionSettings();
    [SerializeField] private DebugSettings debug = new DebugSettings();
    public InputSettings input = new InputSettings();
    public OrbitSettings orbit = new OrbitSettings();
    public PositionSettings position = new PositionSettings();
    public Transform target;

    public Vector2 Rotation
    {
        get { return _orbit.Rotation; }
        set { _orbit.Rotation = value; }
    }

    private void FixedUpdate()
    {
        MoveToTarget();
        LookAtTarget();
        OrbitTarget();

        var t = transform;
        var rotation = t.rotation;
        Collision.UpdateClipPoints(_camera, t.position, rotation, ref _collision.AdjustedClipPoints);
        Collision.UpdateClipPoints(_camera, _destination, rotation, ref _collision.DesiredClipPoints);

        for (var i = 0; i < 5; i++)
        {
            if (debug.drawAdjustedCollisionLines)
            {
                Debug.DrawLine(_position.DesiredPosition, _collision.AdjustedClipPoints[i], Color.green);
            }

            if (debug.drawDesiredCollisionLines)
            {
                Debug.DrawLine(_position.DesiredPosition, _collision.DesiredClipPoints[i], Color.white);
            }
        }

        _collision.CheckColliding(_position.DesiredPosition);
        _position.AdjustedDistance = _collision.GetAdjustedDistance(_position.DesiredPosition);
    }

    private void GetInput()
    {
        if (UnityEngine.Input.GetKeyDown(KeyCode.Q))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (input.enableLook)
        {
            _input.OrbitHorizontal = UnityEngine.Input.GetAxis(input.orbitHorizontal);
            _input.OrbitVertical = UnityEngine.Input.GetAxis(input.orbitVertical);
        }
        else
        {
            _input.OrbitHorizontal = 0.0f;
            _input.OrbitVertical = 0.0f;
        }

        if (input.enableSnap)
        {
            _input.OrbitSnapLeft = UnityEngine.Input.GetButtonDown(input.orbitSnapLeft);
            _input.OrbitSnapRight = UnityEngine.Input.GetButtonDown(input.orbitSnapRight);
        }
        else
        {
            _input.OrbitSnapLeft = false;
            _input.OrbitSnapRight = false;
        }
    }

    private void LookAtTarget()
    {
        var targetRotation = Quaternion.LookRotation(_position.DesiredPosition - transform.position);

        transform.rotation =
            Quaternion.Lerp(transform.rotation, targetRotation, position.smoothLookStrength * Time.deltaTime);
    }

    private void MoveToTarget()
    {
        _position.DesiredPosition = target.position + Vector3.up * position.positionOffset.y +
                                    Vector3.forward * position.positionOffset.z +
                                    transform.TransformDirection(Vector3.right * position.positionOffset.x);

        _destination = Quaternion.Euler(_orbit.Rotation.x,
                           orbit.followTarget ? _orbit.Rotation.y + target.eulerAngles.y : _orbit.Rotation.y, 0.0f) *
                       Vector3.back *
                       position.distance;
        _destination += _position.DesiredPosition;

        var adjustedDestination = _destination;

        if (_collision.IsColliding)
        {
            adjustedDestination = Quaternion.Euler(_orbit.Rotation.x,
                                      orbit.followTarget ? _orbit.Rotation.y + target.eulerAngles.y : _orbit.Rotation.y,
                                      0.0f) * Vector3.forward *
                                  _position.AdjustedDistance;
            adjustedDestination += _position.DesiredPosition;
        }


        transform.position = position.smoothFollow
            ? Vector3.SmoothDamp(transform.position, adjustedDestination, ref _velocity,
                position.smoothFollowStrength)
            : adjustedDestination;
    }

    private void OrbitTarget()
    {
        _orbit.Rotation.x += -_input.OrbitVertical * orbit.verticalSpeed * Time.deltaTime;
        _orbit.Rotation.y += _input.OrbitHorizontal * orbit.horizontalSpeed * Time.deltaTime;

        if (_input.OrbitSnapLeft)
        {
            _orbit.Rotation.y -= 90.0f;
        }

        if (_input.OrbitSnapRight)
        {
            _orbit.Rotation.y += 90.0f;
        }

        _orbit.Rotation.x = Mathf.Clamp(_orbit.Rotation.x, orbit.minVerticalRotation,
            orbit.maxVerticalRotation);
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        _camera = GetComponent<Camera>();
        _collision = new Collision(collision);
        _input = new Input();
        _orbit = new Orbit(orbit);
        _position = new Position(position);

        MoveToTarget();

        {
            var t = transform;
            var rotation = t.rotation;

            Collision.UpdateClipPoints(_camera, t.position, rotation, ref _collision.AdjustedClipPoints);
            Collision.UpdateClipPoints(_camera, _destination, rotation, ref _collision.DesiredClipPoints);
        }
    }

    private void Update()
    {
        GetInput();
    }

    #region Nested type: Collision

    private class Collision
    {
        private readonly LayerMask _collisionLayer;
        public Vector3[] AdjustedClipPoints;
        public Vector3[] DesiredClipPoints;

        public Collision(CollisionSettings collisionSettings)
        {
            _collisionLayer = collisionSettings.collisionLayer;
            AdjustedClipPoints = new Vector3[5];
            DesiredClipPoints = new Vector3[5];
        }

        public bool IsColliding { get; private set; }

        public void CheckColliding(Vector3 targetPosition)
        {
            IsColliding = DesiredClipPoints.Any(clipPoint => Physics.Raycast(targetPosition,
                clipPoint - targetPosition, Vector3.Distance(clipPoint, targetPosition), _collisionLayer));
        }

        public float GetAdjustedDistance(Vector3 from)
        {
            var distance = float.MaxValue;

            foreach (var desiredCameraClipPoint in DesiredClipPoints)
            {
                RaycastHit hit;
                if (!Physics.Raycast(from, desiredCameraClipPoint - from, out hit))
                {
                    continue;
                }

                if (hit.distance < distance)
                {
                    distance = hit.distance;
                }
            }

            return Utility.FloatCompare(distance, float.MaxValue) ? 0.0f : distance;
        }

        public static void UpdateClipPoints(Camera camera, Vector3 cameraDesiredPosition,
            Quaternion rotation, ref Vector3[] result)
        {
            var z = camera.nearClipPlane;
            var x = Mathf.Tan(camera.fieldOfView / 3.41f) * z;
            var y = x / camera.aspect;

            result[0] = rotation * new Vector3(-x, y, z) + cameraDesiredPosition;
            result[1] = rotation * new Vector3(x, y, z) + cameraDesiredPosition;
            result[2] = rotation * new Vector3(-x, -y, z) + cameraDesiredPosition;
            result[3] = rotation * new Vector3(x, -y, z) + cameraDesiredPosition;
            result[4] = cameraDesiredPosition - camera.transform.forward;
        }
    }

    #endregion

    #region Nested type: CollisionSettings

    [Serializable]
    public class CollisionSettings
    {
        public LayerMask collisionLayer;
    }

    #endregion

    #region Nested type: DebugSettings

    [Serializable]
    public class DebugSettings
    {
        public bool drawAdjustedCollisionLines;
        public bool drawDesiredCollisionLines;
    }

    #endregion

    #region Nested type: Input

    private class Input
    {
        public float OrbitHorizontal;
        public bool OrbitSnapLeft;
        public bool OrbitSnapRight;
        public float OrbitVertical;
    }

    #endregion

    #region Nested type: InputSettings

    [Serializable]
    public class InputSettings
    {
        public bool enableLook = true;
        public bool enableSnap;
        public string orbitHorizontal = "OrbitHorizontal";
        public string orbitSnapLeft = "OrbitSnapLeft";
        public string orbitSnapRight = "OrbitSnapRight";
        public string orbitVertical = "OrbitVertical";
    }

    #endregion

    #region Nested type: Orbit

    private class Orbit
    {
        public Vector2 Rotation;

        public Orbit(OrbitSettings orbitSettings)
        {
            Rotation = orbitSettings.initialRotation;
        }
    }

    #endregion

    #region Nested type: OrbitSettings

    [Serializable]
    public class OrbitSettings
    {
        public bool followTarget;
        public float horizontalSpeed = 150.0f;
        public Vector2 initialRotation = new Vector2(-20.0f, 180.0f);
        public float maxVerticalRotation = 25.0f;
        public float minVerticalRotation = -85.0f;
        public float verticalSpeed = 150.0f;
    }

    #endregion

    #region Nested type: Position

    private class Position
    {
        public float AdjustedDistance;
        public Vector3 DesiredPosition = Vector3.zero;

        public Position(PositionSettings positionSettings)
        {
            AdjustedDistance = positionSettings.distance;
        }
    }

    #endregion

    #region Nested type: PositionSettings

    [Serializable]
    public class PositionSettings
    {
        public float distance = -8.0f;
        public Vector3 positionOffset = new Vector3(0.0f, 3.4f, 0.0f);
        public bool smoothFollow = true;
        public float smoothFollowStrength = 0.05f;
        public float smoothLookStrength = 100.0f;
    }

    #endregion
}
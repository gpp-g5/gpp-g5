﻿using UnityEngine;

public class Destroy : MonoBehaviour
{
    public float lifeTime = 3.0f;

    private void Start()
    {
        Destroy(gameObject, lifeTime);
    }
}
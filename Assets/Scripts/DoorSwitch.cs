﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour
{
    public GameObject cutsceneCamera;
    public GameObject mainCamera;
    public Transform focus;
    public Transform cameraDestination;

    private PlayerController player;

    private bool coroutineCalled = false;

    public Animator doorAnim;

    public GameObject buttonUI;

    private void Start()
    {
        player = FindObjectOfType<PlayerController>();

        cutsceneCamera.SetActive(false);
        buttonUI.SetActive(false);
    }

    [Range(1.0f, 10.0f)]
    public float cameraSpeed = 5f;

    private float speedMultiplier = 0.0001f;

    public IEnumerator StartCutscene()
    {
        buttonUI.SetActive(false);
        Animator playerAnim = player.GetComponent<Animator>();
        player.FreezePlayer(true);
        playerAnim.SetFloat("speed", 0);

        cutsceneCamera.SetActive(true);

        cutsceneCamera.transform.position = mainCamera.transform.position;
        cutsceneCamera.transform.rotation = mainCamera.transform.rotation;

        mainCamera.SetActive(false);

        Quaternion startRot = cutsceneCamera.transform.rotation;
        float y = 0f;
        float startDistance = Vector3.Distance(cutsceneCamera.transform.position, cameraDestination.position);

        while (Vector3.Distance(cutsceneCamera.transform.position, cameraDestination.position) > 0.2f)
        {
            cutsceneCamera.transform.position = Vector3.Lerp(cutsceneCamera.transform.position, cameraDestination.position, y += (cameraSpeed * speedMultiplier));

            float x = 1 - Vector3.Distance(cutsceneCamera.transform.position, cameraDestination.position) / startDistance;
            float lerp = 1 / (1 + Mathf.Exp((-12f * (x - 0.5f))));
            cutsceneCamera.transform.rotation = Quaternion.Lerp(startRot, cameraDestination.transform.rotation, lerp);

            yield return null;
        }


        Vector3 destination = new Vector3(focus.position.x, player.transform.position.y, focus.position.z);

        while (Vector3.Distance(player.transform.position, destination) > 0.2f)
        {
            player.transform.LookAt(new Vector3(focus.position.x, player.transform.position.y, focus.position.z));
            playerAnim.SetFloat("speed", 0.4f);
            player.transform.position = Vector3.MoveTowards(player.transform.position, destination, 0.04f);
            yield return null;
        }

        playerAnim.SetFloat("speed", 0);
        playerAnim.SetTrigger("pressButton");



        startRot = cutsceneCamera.transform.rotation;
        y = 0f;

        startDistance = Vector3.Distance(cutsceneCamera.transform.position, mainCamera.transform.position);
        while (Vector3.Distance(cutsceneCamera.transform.position, mainCamera.transform.position) > 0.2f)
        {
            cutsceneCamera.transform.position = Vector3.Lerp(cutsceneCamera.transform.position, mainCamera.transform.position, y += (cameraSpeed * speedMultiplier));

            float x = 1 - Vector3.Distance(cutsceneCamera.transform.position, mainCamera.transform.position) / startDistance;
            float lerp = 1 / (1 + Mathf.Exp((-12f * (x - 0.5f))));
            cutsceneCamera.transform.rotation = Quaternion.Lerp(startRot, mainCamera.transform.rotation, lerp);

            yield return null;
        }

        playerAnim.SetFloat("speed", 0);

        ResetCameras();
    }

    public void ResetCameras()
    {
        player.FreezePlayer(false);
        mainCamera.SetActive(true);
        cutsceneCamera.SetActive(false);

        coroutineCalled = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject == player.gameObject && !coroutineCalled)
        {
            buttonUI.SetActive(true);

            if(Input.GetButtonDown("Interact"))
            {
                StartCoroutine(StartCutscene());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player.gameObject && !coroutineCalled)
        {
            buttonUI.SetActive(false);
        }
    }

    public void PushButton()
    {
        GetComponent<Animator>().SetTrigger("pressed");
        doorAnim.SetTrigger("open");
    }
}

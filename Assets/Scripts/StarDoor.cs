﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarDoor : MonoBehaviour
{
    public string scene = "GameScene_2";
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = FindObjectOfType<PlayerController>();
        SceneTransition sceneTransition = FindObjectOfType<SceneTransition>();

        if(other.gameObject == player.gameObject)
        {
            sceneTransition.LoadScene(scene);
        }
    }

    public void OpenDoor()
    {
        anim.SetTrigger("open");
    }


}

﻿using UnityEngine;

public class SpeedBoost : PowerUp
{
    public float speedMultiplier = 3;
    public GameObject playerPS;
    private GameObject ps;

    private Vector3 offset;


    public override void PickUp()
    {
        SetMovementSpeed(speedMultiplier);
        player.speedBoost = this;

        offset = new Vector3(0, 1, 0);
        ps = Instantiate(playerPS, player.transform.position + offset, Quaternion.identity);
        ps.transform.parent = player.transform;
        ps.transform.localRotation = Quaternion.Euler(0, -180, 0);
    }

    public override void ResetPowerUp()
    {
        if (player.speedBoost == this)
        {
            SetMovementSpeed(1 / speedMultiplier);
            Destroy(ps);
        }
    }

    public void SetMovementSpeed(float multiplier)
    {
        player.runSpeed *= multiplier;
        player.walkSpeed *= multiplier;

        player.GetComponent<Animator>().speed *= multiplier;
    }
}
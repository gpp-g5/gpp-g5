﻿using UnityEngine;

public class DoubleJump : PowerUp
{

    public GameObject playerPs;
    private GameObject ps;

    public override void PickUp()
    {
        player.extraJumps = 1;
        player.extraJumpCount = player.extraJumps;
        player.doubleJump = this;
    }

    public override void ResetPowerUp()
    {
        if (player.doubleJump == this)
        {
            player.extraJumps = 0;
            player.extraJumpCount = player.extraJumps;
        }
    }

    public void SpawnEffect()
    {
        Instantiate(playerPs, player.transform);
    }
}
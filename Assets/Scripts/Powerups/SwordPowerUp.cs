﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordPowerUp : PowerUp
{
    public float speedMultiplier = 3;

    public override void PickUp()
    {
        player.HasSword(1);
        player.swordPowerUp = this;
    }

    public override void ResetPowerUp()
    {
        if (player.swordPowerUp == this)
        {
            player.HasSword(0);
        }
    }
}

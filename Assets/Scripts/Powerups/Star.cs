﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : PowerUp
{
    public override void PickUp()
    {
        FindObjectOfType<StarUI>().StarCollected();
    }

    public override void ResetPowerUp()
    {

    }
}

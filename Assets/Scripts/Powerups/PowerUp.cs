﻿using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public float amplitude = .01f;

    public bool canRespawn = true;

    private bool collected = false;

    public GameObject collectEffect;
    private float elapsedTimeDuration;
    private float elapsedTimeRespawn;
    public float frequency = 1.0f;

    [HideInInspector] public bool hasPowerUp = false;

    [HideInInspector] public PlayerController player;

    public float powerUpDuration = 15.0f;

    private float respawnTime;
    public float rotationSpeed = 50.0f;

    private void Start()
    {
        player = FindObjectOfType<PlayerController>();
        respawnTime = powerUpDuration + 1.0f;
    }


    private void Update()
    {
        transform.Rotate(rotationSpeed * Time.deltaTime, rotationSpeed * Time.deltaTime,
            rotationSpeed * Time.deltaTime);
        transform.position += new Vector3(0, Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude, 0);

        if (hasPowerUp)
        {
            elapsedTimeDuration += Time.deltaTime;

            if (elapsedTimeDuration > powerUpDuration)
            {
                ResetPowerUp();
                hasPowerUp = false;
            }
        }
        else
        {
            elapsedTimeDuration = 0;
        }

        if (collected && canRespawn)
        {
            elapsedTimeRespawn += Time.deltaTime;

            if (elapsedTimeRespawn > respawnTime)
            {
                EnablePowerUp(true);
                collected = false;
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player.gameObject)
        {
            EnablePowerUp(false);
            Instantiate(collectEffect, transform.position, Quaternion.identity);
            PickUp();
            collected = true;
            hasPowerUp = true;
            elapsedTimeRespawn = 0;
        }
    }

    private void EnablePowerUp(bool enable)
    {
        foreach (Transform child in transform)
        {
            ParticleSystem ps = child.GetComponent<ParticleSystem>();

            if (ps)
            {
                if (enable)
                {
                    ps.Play();
                }
                else
                {
                    ps.Stop();
                }
            }
        }

        Renderer render = GetComponent<Renderer>();
        BoxCollider collider = GetComponent<BoxCollider>();

        if (render)
        {
            render.enabled = enable;
        }

        if (collider)
        {
            collider.enabled = enable;
        }
    }

    public virtual void PickUp()
    {
        //overriden in child class.
    }

    public virtual void ResetPowerUp()
    {
        //overriden in child class.
    }
}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    private Animator transitionAnim;

    private void Start()
    {
        transitionAnim = GetComponent<Animator>();
    }


    public void LoadScene(string sceneName)
    {
        StartCoroutine(Transition(sceneName));
    }

    public void ExitGame()
    {
        StartCoroutine(ExitTransition());
    }

    public void Respawn()
    {
        StartCoroutine(RespawnTransition(SceneManager.GetActiveScene().name));
    }

    IEnumerator Transition(string sceneName)
    {
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }

    IEnumerator ExitTransition()
    {
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1);
        Application.Quit();
    }

    IEnumerator RespawnTransition(string sceneName)
    {
        yield return new WaitForSeconds(3);
        transitionAnim.SetTrigger("end");

        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }
}
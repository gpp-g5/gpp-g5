using UnityEngine;

[RequireComponent(typeof(Collider))]
public class BezierCurveEnter : MonoBehaviour
{
    private BezierCurve _curve;
    [SerializeField] private Vector2 cameraRotation;
    [SerializeField] private float t;

    private void OnTriggerEnter(Collider other)
    {
        var follower = other.GetComponent<IBezierCurveFollower>();
        if (follower == null || follower.Curve == _curve) return;
        follower.CameraRotation = cameraRotation;
        follower.CurrentT = t;
        follower.Curve = _curve;
    }

    private void Start()
    {
        _curve = transform.parent.GetComponent<BezierCurveSpline>().curve;
        t = Mathf.Clamp(t, 0.0f, _curve.SegmentCount);
    }
}
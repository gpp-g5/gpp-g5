﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarUI : MonoBehaviour
{
    private int starsCollected = 0;
    public int maxStars = 10;
    private Slider slider;

    private void Start()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = maxStars;
        slider.minValue = 0;
        slider.value = slider.minValue;
    }

    public void StarCollected()
    {
        starsCollected += 1;
        slider.value = starsCollected;

        if(starsCollected >= maxStars)
        {
            CutsceneManager cutscene = FindObjectOfType<CutsceneManager>();
            StartCoroutine(cutscene.StartCutscene());
        }
    }
}

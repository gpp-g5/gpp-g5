using System;
using UnityEngine;

[ExecuteInEditMode]
public class BezierCurveSpline : MonoBehaviour
{
    public BezierCurve curve = new BezierCurve(BezierCurve.Mode.Cubic);

    private void Awake()
    {
        SyncCurve();
    }

    private void OnDrawGizmos()
    {
        var segmentCount = curve.SegmentCount;
        if (segmentCount == 0)
        {
            return;
        }

        Gizmos.color = Color.cyan;
        for (var t = 0.0f; t <= (float) segmentCount;)
        {
            Gizmos.DrawWireSphere(curve.GetPosition(t), 0.25f);
            t += 0.2f / curve.GetTangent(t).magnitude;
        }

        Gizmos.color = Color.blue;
        switch (curve.currentMode)
        {
            case BezierCurve.Mode.Cubic:
                for (var i = 0; i < segmentCount; i++)
                {
                    var p0Index = i * 3;
                    var p0 = curve.points[p0Index].position;
                    var p1 = curve.points[p0Index + 1].position;
                    var p2 = curve.points[p0Index + 2].position;
                    var p3 = curve.points[p0Index + 3].position;

                    Gizmos.DrawLine(p0, p1);
                    Gizmos.DrawLine(p3, p2);
                }

                break;
            case BezierCurve.Mode.CubicLoop:
                for (var i = 0; i < segmentCount; i++)
                {
                    var p0Index = i * 3;
                    var p0 = curve.points[p0Index].position;
                    var p1 = curve.points[p0Index + 1].position;
                    var p2 = curve.points[p0Index + 2].position;
                    var p3 = curve.points[p0Index + 3 < curve.points.Count ? p0Index + 3 : 0].position;

                    Gizmos.DrawLine(p0, p1);
                    Gizmos.DrawLine(p3, p2);
                }

                break;
            case BezierCurve.Mode.Linear:
            case BezierCurve.Mode.LinearLoop:
                break;
            case BezierCurve.Mode.Quadratic:
                for (var i = 0; i < segmentCount; i++)
                {
                    var p0Index = i * 2;
                    var p0 = curve.points[p0Index].position;
                    var p1 = curve.points[p0Index + 1].position;
                    var p2 = curve.points[p0Index + 2].position;

                    Gizmos.DrawLine(p0, p1);
                    Gizmos.DrawLine(p2, p1);
                }

                break;
            case BezierCurve.Mode.QuadraticLoop:
                for (var i = 0; i < segmentCount; i++)
                {
                    var p0Index = i * 2;
                    var p0 = curve.points[p0Index].position;
                    var p1 = curve.points[p0Index + 1].position;
                    var p2 = curve.points[p0Index + 2 < curve.points.Count ? p0Index + 2 : 0].position;

                    Gizmos.DrawLine(p0, p1);
                    Gizmos.DrawLine(p2, p1);
                }

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void SyncCurve(bool force = false)
    {
        var childCount = transform.childCount;
        if (childCount == curve.points.Count && !force) return;
        Debug.Log("Syncing the curve");
        var currentMode = curve.currentMode;
        curve = new BezierCurve(currentMode);
        for (var i = 0; i < childCount; i++)
        {
            curve.points.Add(transform.GetChild(i).transform);
        }
    }
}